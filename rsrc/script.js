/* configuration */
var updateInterval = 60000;

/*end of config*/
var map;

var mapLink = '<a href="http://openstreetmap.org">OSM</a>';
var myIconPoint = L.icon({
    iconUrl: 'rsrc/images/bluePoint.png',
    iconRetinaUrl: 'rsrc/images/bluePoint.png',
    iconSize: [9, 9],
    iconAnchor: [10, 10],
    popupAnchor: [-3, -76],
    shadowUrl: 'rsrc/images/bluePoint.png',
    shadowRetinaUrl: 'rsrc/images/bluePoint.png',
    shadowSize: [9, 9],
    shadowAnchor: [10, 10]
});

var popup = L.popup();
var lastPoint = null;
var lastMarker = null;
var trackColor = null;

function addPoint(lat, lon, time) {
  var d = new Date(time * 1000);

  var currentPoint = L.latLng(lat, lon);
  lastMarker = L.marker(currentPoint, {title: d.toString(), icon:myIconPoint}).addTo(map);

  if (lastPoint != null) {
    var polyline = L.polyline([currentPoint, lastPoint], {color: trackColor}).addTo(map);
  }

  lastPoint = currentPoint;
 
}

function getRandomColor() {
  return '#'+Math.floor(Math.random()*16777215).toString(16);
}

function newTrack() {
  lastPoint = null;
  trackColor = getRandomColor();
}

function onMapClick(e) {
  if ( $('#isRecording').val() == 'true') {
    $.get('gps.php?action=addData&lat='+e.latlng.lat+'&lon='+e.latlng.lng);

    popup
      .setLatLng(e.latlng)
      .setContent("You added the point at " + e.latlng.toString())
      .openOn(map);


  }
}



function getLastCoords() {
  $.getJSON(
      'gps.php?action=getTracksUpdates&last='+$("#lastUpdate").val(),
      function (data) {
        $("#lastUpdate").val(data.srvTime);
        console.log(data.msg);

       
        readTrack(data.data);
        
        window.setTimeout(getLastCoords, updateInterval);

      }
    );


}

function readTrack(data) {
  $.each (data, function(key) {

    if (data[key].type == 'tracker') {
      newTrack();
    }

    if (data[key].type == 'coord') {
      addPoint(data[key].lat, data[key].lon, data[key].time);
    }

  });

  //Go to last point added
  if (lastPoint != null) {
    L.panTo(lastPoint);
  }
}

function getAllCoords() {
  $.getJSON(
      'gps.php?action=getAllTracks',
      function (data) {
        console.log(data.msg);
        $("#lastUpdate").val(data.srvTime);
        newTrack();
        readTrack(data.data);
      }
    );

  window.setTimeout(getLastCoords, updateInterval);
}

function recordTrack() {
  if ( $('#isRecording').val() == 'false') {
    console.log('start recording...');
    $('#isRecording').val('true');
    $.get('gps.php?action=addData&tracker=start');
  } else {
    console.log('stop recording.');
    $('#isRecording').val('false');
    $.get('gps.php?action=addData&tracker=stop');
  }
}

function resetAll() {
  $.get('gps.php?action=resetAll', function(data) {
    location.reload();
  });
}

$( function(){
  map = L.map('map').setView([48.692, 2.5201], 12);
  L.tileLayer(
    'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; ' + mapLink,
    maxZoom: 18,
  }).addTo(map);


  map.on('click', onMapClick);

  getAllCoords();
  $('#isRecording').val('false');

  L.easyButton('&CirclePlus;', function(btn, map){
    recordTrack();
  }).addTo( map ); 

  L.easyButton('&CircleTimes;', function(btn, map){
    if (window.confirm("Are you sure you want to delete all tracks recorded?")) {
        resetAll();
    }
  }).addTo( map ); 


});


