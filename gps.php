<?php
$configFile = 'data/config.json';

if (isset($_GET['action'])) {
   $action = $_GET['action'];
} else {
   $action = '';
}

function readConfig($file) {
  $data = file_get_contents($file);
  if (!$data) {
    return false;
  }

  $config = json_decode ($data, true);
  return $config;

}

function outputResult($result) {
   $result['srvTime'] = time();
   echo json_encode($result);
}

function emptyFile() {
  global $config;

  $f = fopen ($config['dataFile'], 'w');
  if ($f == true) {
    fclose($f);
  } else {
    return array('r'=> 2, 'msg' => 'Error opening file');
  }
  return array('r'=> 0, 'msg' => 'Records emptied');

}

function addPosition() {
  global $config, $_GET, $_SERVER;
  $result = array();
  $data = array();

  $data['time'] = time();
  $data['emiter'] = $_SERVER['REMOTE_ADDR'];
  $data['request'] = $_SERVER['REQUEST_URI'];
  if (isset($_GET['lat']) && preg_match("/^-?\d+.\d+$/", $_GET["lat"]) && isset($_GET['lon']) && preg_match("/^-?\d+.\d+$/", $_GET["lon"])) {
    $data['coord_lat'] = $_GET['lat'];
    $data['coord_lon'] = $_GET['lon'];
  }
  if (isset($_GET['tracker'])) {
    $data['tracker'] = $_GET['tracker'];
  }
  if (empty($data['tracker']) && empty($data['coord_lat']) && empty($data['coord_lon'])) {
    return array('r'=> 1, 'msg' => 'No data to save');
  }

  $line = json_encode($data) . "\n";

  if (!is_file ($config['dataFile'])) {
    touch($config['dataFile']);
  }

  $f = fopen ($config['dataFile'], 'a');
  if ($f == true) {
    fwrite ($f, $line);
    fclose($f);
  } else {
    return array('r'=> 2, 'msg' => 'Error opening file');
  }
  return array('r'=> 0, 'msg' => 'saved');

}

function getTracks($lastTime = 0) {
  global $config;
    $output = array();
    $lines = file ($config['dataFile'], FILE_SKIP_EMPTY_LINES );

    if ($lines == false) {
      return array('r'=> 2, 'msg' => 'Error opening file');
    }
    foreach ($lines as $line) {
       $data = json_decode($line, true);

       if ($data['time'] < $lastTime) {
          continue;
       }
       if (isset($data['tracker'])) {
         $output[] = array(
                     'time'     => $data['time'],
                     'type'     => 'tracker',
                     'value'    => $data['tracker']
                     );
 
       } elseif (isset($data['coord_lat']) && isset($data['coord_lon']))  {
         $output[] = array(
                     'time'     => $data['time'],
                     'type'     => 'coord',
                     'lat'      => $data['coord_lat'],
                     'lon'      => $data['coord_lon']
                     );
       } else {
         continue;
       }
    }
    return array('r'=> 0, 'msg' => 'Tracks retreived', 'data' => $output);
}

$config = readConfig($configFile);
switch($action) {
  default:
  case 'addData':
    $result = addPosition();
    break;

  case 'resetAll':
    $result = emptyFile();
    break;

  case 'getTracksUpdates':

    $result = getTracks($_GET['last']);   

    break;

  case 'getAllTracks':
    $result = getTracks();   
   
    break;

}

outputResult($result);
?>
